    <footer id="footer" class="inner">
        <div id="footer-top" class="section group">
            <div id="footer-top-col1" class="col span_2_3">
                <img src="<?php echo get_template_directory_uri(); ?>/images/logo-footer.png">
                <nav>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'menu-footer' ) ); ?>
                </nav>
                <?php
                if (is_active_sidebar('footer-slogan')) :
                    dynamic_sidebar('footer-slogan');
                endif;
                ?>  
            </div>
            <div id="footer-top-col2" class="col span_1_3">
                <div>
                    <?php
                    if (is_active_sidebar('footer-contact-us')) :
                        dynamic_sidebar('footer-contact-us');
                    endif;
                    ?>                      
                </div>      
            </div>
        </div>
        <div id="footer-bot">
            <?php
            if (is_active_sidebar('footer-copyright')) :
                dynamic_sidebar('footer-copyright');
            endif;
            ?>              
        </div>
    </footer>
</div>
<?php  if (is_home()): ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        jQuery(window).load(function() {
            jQuery('#slideshow').flexslider({
                animation: "fade",
                direction: "horizontal",
                slideshowSpeed: 7000,
                animationSpeed: 600,
                directionNav: true,
                initDelay: 0,
                prevText: "",           
                nextText: "",
            });
        });
    </script>       
<?php endif ?>
</body>
</html>