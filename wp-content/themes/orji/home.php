<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

    <div id="home-content" class="inner">
        <div id="main" class="section group">
            <div id="main-col1" class="col span_2_4">
                <?php 
                // Query Arguments
                $args = array(
                    'post_type' => array('news','post','press','events'),
                    'posts_per_page' => 5,
                    'category_name' => 'featured',
                );  
         
                // The Query
                $the_query = new WP_Query( $args );

                if ( $the_query->have_posts() ):
                ?>
                    <div id="slideshow" class="flexslider">
                        <ul class="slides">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <?php if (has_post_thumbnail()): ?>
                            <li>
                                <div class="image">
                                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'homepage-slider' ); ?>
                                        <img src="<?php echo $image[0] ?>">
                                        <div class="text">
                                            <h1><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
                                            <?php the_excerpt(); ?>
                                            <a href="<?php echo get_permalink(); ?>" class="read-more"><div class="arrow-right"></div>Read More</a>
                                        </div>                                        
                                </div>
                            </li>
                            <?php endif ?>
                        <?php endwhile; ?>
                    </div>

                <?php endif ?>
                <section id="news">
                    <div id="title-wrapper" class="section group">
                        <h1>Editorials & Interviews</h1>
                        <div class="view-all"><a href="news">View all</a></div>
                    </div>
                    <?php
                    $args = array(
                        'post_type' => array('news'),
                        'posts_per_page' => 4,
                        'category__not_in' => array(7,1)
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>                         
                    <article class="section group">
                        <?php if (has_post_thumbnail()): ?>
                            <div class="col span_1_4">
                            <?php the_post_thumbnail('post-thumbnails'); ?>
                            </div>
                            <aside class="col span_3_4">
                        <?php else: ?>
                            <aside class="col span_4_4">
                        <?php endif ?>    
                            <header>
                                <?php the_category(); ?>
                                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                            </header>
                            <?php the_excerpt(); ?>
                            <div class="read-more"><div class="arrow-right"></div><a href="<?php the_permalink(); ?>">Read More</a></div>
                            <div class="share"></div>
                        </aside>            
                    </article>      
                    <?php endwhile ?>     
                </section>
            </div>
            <section id="press" class="col span_1_4">
                <h1>Int'l Statements</h1>
                <?php
                $args = array('posts_per_page' => 4,'post_type' => 'press');

                $posts_array = get_posts( $args );

                foreach ( $posts_array as $post ): 
                ?>                   
                <article>
                    <header>
                        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    </header>
                    <?php the_excerpt(); ?>
                    <div class="read-more"><div class="arrow-right"></div><a href="<?php the_permalink(); ?>">Read More</a></div>           
                </article>
                <?php endforeach ?>                                                                            
                <div class="view-all"><a href="press">View all</a></div>     
            </section>
            <div id="main-col3" class="col span_1_4">
                <section id="events">
                    <h1>Events</h1>
                    <?php
                    $args = array('posts_per_page' => 3,'post_type' => 'events');

                    $posts_array = get_posts( $args );

                    foreach ( $posts_array as $post ): 
                    ?>                      
                    <article>
                        <time class="date">
                            <?php $date = get_post_custom_values('event_date') ?>    
                            <div class="day"><?php echo date("d", strtotime($date[0])) ?></div>
                            <div class="month"><?php echo date("M", strtotime($date[0])) ?></div>
                            <div class="year"><?php echo date("Y", strtotime($date[0])) ?></div>
                        </time>
                        <aside>
                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                            <?php the_excerpt(); ?>
                            <div class="read-more"><div class="arrow-right"></div><a href="<?php the_permalink(); ?>">Read More</a></div>
                        </aside>        
                    </article>
                    <?php endforeach ?>               
                    <div class="view-all"><a href="events">View all</a></div>                 
                </section>
                <section id="boxes">
                    <div id="newsletter-box">
                        <p>Subscribe to Newsletter</p>
                        <?php
                        if (is_active_sidebar('homepage-newsletter')) :
                            dynamic_sidebar('homepage-newsletter');
                        endif;
                        ?>
                    </div>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/img-quote.jpg">
                    <div class="quote">
                        <div class="quote-left"></div>
                        <?php
                        if (is_active_sidebar('homepage-quote')) :
                            dynamic_sidebar('homepage-quote');
                        endif;
                        ?>
                        <div class="quote-right"></div>
                    </div>
                    <div id="buttons-col3">
                        <div class="button-brown">
                            <?php
                            if (is_active_sidebar('homepage-button1')) :
                                dynamic_sidebar('homepage-button1');
                            endif;
                            ?>
                        </div>
                        <div class="separator"></div>
                        <div class="button-blue">
                            <?php
                            if (is_active_sidebar('homepage-button2')) :
                                dynamic_sidebar('homepage-button2');
                            endif;
                            ?>                        
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <section id="multimedia" class="section group">
            <div id="multimedia-title">
                <h1>Multimedia</h1>
                <div class="view-all"><a href="multimedia">View all</a></div> 
            </div>
            <div id="images">
                <?php
                $args = array('posts_per_page' => 5,'post_type' => 'multimedia');

                $posts_array = get_posts( $args );

                foreach ( $posts_array as $post ): 
                ?>
                    <?php 
                    $image = get_post_custom_values('image');
                    $video = get_post_custom_values('video');
                    $video_image = get_post_custom_values('video_image');

                    if ($image[0] != ''):
                        $image = wp_get_attachment_image($image[0], 'homepage-multimedia');
                    ?>
                    <li class="col span_1_5 image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/image.jpg" class="icon">
                        <a href="<?php the_permalink(); ?>"><?php echo $image; ?></a>
                    </li>
                    <?php 
                    elseif ($video[0] != ''): 
                        $video_image = $image = wp_get_attachment_image($video_image[0], 'homepage-multimedia');
                    ?>
                    <li class="col span_1_5 video">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/video.jpg" class="icon">
                        <a href="<?php the_permalink(); ?>"><?php echo $video_image ?></a>
                    </li>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
        </section>
        <section id="blog" class="section group">
            <div id="blogwrapper" class="col span_3_4">
                <div id="blogtitle-wrapper" class="col span_2_10">
                    <h1>Blog</h1>
                    <div class="view-all"><a href="<?php echo date('Y/m') ?>">View all</a></div> 
                </div>
                <div id="blogarticle-wrapper" class="col span_8_10">
                    <?php
                    $args = array('posts_per_page' => 4,'post_type' => 'post');

                    $posts_array = get_posts( $args );

                    $i=0;
                    $total_posts = count($posts_array);

                    foreach ( $posts_array as $post ): 
                        $i++;
                    ?>  
                        <?php if ($i==1): ?>
                            <div id="blog-col1" class="col span_1_2">
                        <?php elseif ($i==3): ?>
                            <div id="blog-col2" class="col span_1_2" class="col span_1_4">
                        <?php endif ?>
                            <article>
                                <?php if (has_post_thumbnail()): ?><div class="col span_1_4"><?php the_post_thumbnail('homepage-blog'); ?></div><?php endif ?>
                                <?php if (has_post_thumbnail()): ?>
                                <aside class="col span_3_4">
                                <?php else: ?>
                                <aside class="col span_4_4">
                                <?php endif ?>
                                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                                    <?php the_excerpt() ?>
                                    <div class="read-more"><div class="arrow-right"></div><a href="<?php the_permalink(); ?>">Read More</a></div>
                                </aside>
                            </article>
                        <?php if ( $total_posts==1 || ($total_posts==3 && $i==3) || $i==2 || $i==4): ?>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
            <div id="buttons-blog" class="col span_1_4">
                <div class="button-brown">
                    <?php
                    if (is_active_sidebar('homepage-button3')) :
                        dynamic_sidebar('homepage-button3');
                    endif;
                    ?>                    
                </div>
                <div class="separator"></div>
                <div class="button-blue">
                    <?php
                    if (is_active_sidebar('homepage-button4')) :
                        dynamic_sidebar('homepage-button4');
                    endif;
                    ?>                    
                </div>
            </div>                                      
        </section>
    </div>

<script type="text/javascript">
function updateLayout(){
    var totalwidth = jQuery('.quote').width();
    var widthcontent = totalwidth - 60;
    jQuery('.quote .textwidget').css('width', widthcontent);
    jQuery('.quote-right').css('height',jQuery('.quote .textwidget').height());
    jQuery('#main-col3').css('height',jQuery('#press').height());
}
jQuery(document).ready(function () {
    updateLayout();
    jQuery(window).resize(function(){
        updateLayout();
    });
});
</script>    

<?php get_footer(); ?>    