<?php

get_header(); ?>

	<section id="primary" class="content-area inner">

        <div id="content" class="site-content has-sidebar col span_5_6" role="main">

			<header class="archive-header-category">
				<?php if (get_post_type() == ''): ?>
					<h1 class="archive-title"><?php printf( single_cat_title( '', false ) ); ?></h1>
				<?php else: ?>
					<h1><?php echo ucfirst(get_post_type()) ?></h1>
				<?php endif ?>
			</header><!-- .archive-header -->

			<div id="content-multimedia">
	        <?php
	        $cur_cat = get_cat_ID( single_cat_title("",false) );

			echo($category_id);

	        $args = array('posts_per_page' => 15,'post_type' => 'multimedia', 'paged' => get_query_var('paged'));

	        $posts_array = get_posts( $args );

	        foreach ( $posts_array as $post ): 
	        ?>   
	    		<div class="multimedia-element col span_1_4">
                <?php 
                $image = get_post_custom_values('image');
                $video = get_post_custom_values('video');
                $video_image = get_post_custom_values('video_image');

                if ($image[0] != ''):
                    $image = wp_get_attachment_image($image[0], 'large');
                ?>
                	<img src="<?php echo get_template_directory_uri(); ?>/images/image.jpg" class="icon">
                	<a href="<?php the_permalink(); ?>"><?php echo $image; ?></a>
                <?php 
                elseif ($video[0] != ''): 
                    $video_image = $image = wp_get_attachment_image($video_image[0], 'large');
                ?>
                	<img src="<?php echo get_template_directory_uri(); ?>/images/video.jpg" class="icon">
                	<a href="<?php the_permalink(); ?>"><?php echo $video_image ?></a>
                <?php endif ?>
                </div>
			<?php endforeach; ?>	
			</div>	

			<?php wp_pagenavi( ); ?>

		</div><!-- #content -->

		<?php if ( is_active_sidebar( 'sidebar-news' ) ) : ?>
			<div id="secondary" class="widget-area col span_1_6" role="complementary">
				<?php dynamic_sidebar( 'sidebar-news' ); ?>
			</div><!-- #secondary -->
		<?php endif; ?>	

	</section><!-- #primary -->

<?php get_footer(); ?>