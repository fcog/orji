<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if (get_post_type() == 'post'): ?>
	<header class="entry-header-post">
	<?php else: ?>
	<header class="entry-header">
	<?php endif ?>
		<?php //if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<!-- <div class="entry-thumbnail"> -->
			<?php //the_post_thumbnail('header-image'); ?>
		<!-- </div> -->
		<?php //endif; ?>

		<h1 class="entry-title"><?php the_title(); ?></h1>

		<?php $date = get_post_custom_values('event_date') ?>  
		<strong><?php _e('Event date') ?>:</strong> <time><?php echo date("F j, Y", strtotime($date[0])) ?></time>
		</p>

	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'slok' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php comments_template( '', true ); ?>

	</footer><!-- .entry-meta -->
</article><!-- #post -->
