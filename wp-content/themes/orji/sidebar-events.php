	<?php if ( is_active_sidebar( 'sidebar-events' ) ) : ?>
		<div id="secondary" class="widget-area col span_1_6" role="complementary">
			<?php dynamic_sidebar( 'sidebar-events' ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>