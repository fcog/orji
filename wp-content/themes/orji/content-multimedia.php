<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<!-- <div class="entry-thumbnail"> -->
			<?php //the_post_thumbnail('header-image'); ?>
		<!-- </div> -->
		<?php //endif; ?>
		<h1>Multimedia</h1>
		<h2 class="entry-title"><?php the_title(); ?></h2>

		<?php if (get_post_type() != 'page'): ?>
		<p class="entry-date">
			<?php _e('Posted') ?> <time><?php the_time('F j, Y') ?></time>
		</p>
		<?php endif ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content(); ?>
        <?php 
        $image = get_post_custom_values('image');
        $video = get_post_custom_values('video');
        $video_image = get_post_custom_values('video_image');

        if ($image[0] != ''):
            $image = wp_get_attachment_image($image[0], 'large');
        ?>
        <?php echo $image; ?>
        <?php 
        elseif ($video[0] != ''): 
            $video_image = $image = wp_get_attachment_image($video_image[0], 'large');
			$url = "http://www.youtube.com/watch?v=C4kxS1ksqtw&feature=relate";
			parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        ?>
        <div class="video-container">
        <iframe width="420" height="315" src="//www.youtube.com/embed/<?php echo $my_array_of_vars['v']; ?>" frameborder="0" allowfullscreen></iframe>
    	</div>
        <?php endif ?>		
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php comments_template( '', true ); ?>

	</footer><!-- .entry-meta -->
</article><!-- #post -->
