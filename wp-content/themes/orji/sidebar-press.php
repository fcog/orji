	<?php if ( is_active_sidebar( 'sidebar-press' ) ) : ?>
		<div id="secondary" class="widget-area col span_1_6" role="complementary">
			<?php dynamic_sidebar( 'sidebar-press' ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>